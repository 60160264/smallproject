const express = require('express');
const router = express.Router();
const customersController = require('../controller/CustomersController')

/* GET users listing. */
router.get('/', customersController.getUsers);

router.get('/:id', customersController.getUser)

router.post('/', customersController.addUser)

router.put('/', customersController.updateUser)

router.delete('/:id', customersController.deleteUser)

module.exports = router;
